package com.example.practicaprexamenc1java;

public class ReciboNomina {
    //Declaracion de variables necesarias
    private int numRecibo;
    private String nombre;
    private float horasTrabExtras;
    private float horasTrabnormal;
    private int puesto;
    private float impuestoPorc;

    //Creacion del contructor
    ReciboNomina(int numRecibo, String nombre, float horasTrabnormal, float horasTrabExtras,
                 int puesto, float impuestoPorc){
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabExtras = horasTrabExtras;
        this.horasTrabnormal = horasTrabnormal;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    //Set y Getter de las variables
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabnormal() {
        return horasTrabnormal;
    }

    public void setHorasTrabnormal(float horasTrabnormal) {
        this.horasTrabnormal = horasTrabnormal;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    //Funciones necesarias
    public float Calcular(float pago){
        float calculo;
        calculo = pago * horasTrabnormal;
        if (horasTrabExtras > 0){
            calculo = calculo + (horasTrabExtras * pago * 2);
        }
        return  calculo;
    }

    //Funcion para calcular el Subtotal
    public float calcularSubtotal(){
        float subTotal = 0.0F;
        //Comprobacion del puesto
        switch (puesto){
            case 1: subTotal = Calcular(240);
                    return subTotal;
            case 2: subTotal = Calcular(300);
                    return subTotal;
            case 3: subTotal = Calcular(400);
                    return subTotal;
            default: return subTotal;
        }
    }

    //Funcion para calcular el Impuesto
    public float calcularImpuesto(float subTotal){
        impuestoPorc = subTotal * 0.16F;
        return impuestoPorc;
    }

    //Funcion para calcular el Total a pagar al empleado
    public float calcularTotal(float impuesto, float subTotal) {
        return subTotal - impuesto;
    }

}