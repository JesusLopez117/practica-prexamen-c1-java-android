package com.example.practicaprexamenc1java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Variables de entrada
    private EditText txtNom;

    //Variables de tipo boton
    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Salir();
            }
        });
    }

    //Funcion para hacer la relacion de los elementos
    private void iniciciarComponentes(){
        txtNom = findViewById(R.id.txtNom);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    //Funcion para entrar al sistema.
    private  void Ingresar(){
        //Validacion de ingreso de campos
        if(txtNom.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),
                    "Ingrese un nombre", Toast.LENGTH_LONG).show();
        }else{
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNom.getText().toString());
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            txtNom.setText("");
        }
    }

    //Funcion para salir de la aplicacion
    private void Salir(){
        finish();
    }
}